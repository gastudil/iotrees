/*
 Based on:
   NTP: https://www.geekstips.com/arduino-time-sync-ntp-server-esp8266-udp/
   RF24 Rx: http://forum.arduino.cc/index.php?topic=421081.msg2899627#msg2899627
            https://github.com/nRF24/RF24
*/

#include <ESP8266HTTPClient.h>
#include <ESP8266WiFi.h>
#include <ArduinoJson.h>
#include <WiFiUdp.h>
#include <time.h>
#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>
#include "WiFiSSID.h"

/////////////////////////////////////////////////////////////////////////
// RF24 (ESP Receiver)
#define CE_PIN   2
#define CSN_PIN 15

const byte thisSlaveAddress[5] = {'R','x','I','o','T'};

RF24 radio(CE_PIN, CSN_PIN);
bool newData = false;

struct StationData {
  uint8_t stationId;
  uint8_t sampleNumber;
  uint8_t pulse;
  uint8_t ldr;
  uint8_t soilMoisture;
  int8_t dhtT;
  int8_t dhtTm;
  int8_t dhtH;
  int8_t adc0;
  int8_t adc1;
  int8_t adc2;
  int8_t adc3;
};

struct StationData data;

////////////////////////////////////////////////////////////////////////
//InterSCity host
const char* host = "143.107.45.126";
const int hostPort = 30134;

// InterSCity uuid and resource
//const char* uuid = "e8dcf7f8-4977-4295-a955-3ee3daf18714"; // old_data
//const char* uuid = "d1557e9b-ea3e-45f8-828b-820055230e20"; // sapflow
const char* uuid = "164f4961-5e91-4d10-8e70-6e0df691f798"; // leaves
const char* resource = "room_monitoring";

////////////////////////////////////////////////////////////////////////
// NTP resources

unsigned int localPort = 2390;      // local port to listen for UDP packets
IPAddress timeServerIP; // time.nist.gov NTP server address
const char* ntpServerName = "0.br.pool.ntp.org";
const int NTP_PACKET_SIZE = 48; // NTP time stamp is in the first 48 bytes of the message
byte packetBuffer[ NTP_PACKET_SIZE]; //buffer to hold incoming and outgoing packets

// An UDP instance for sending and receiving packets over UDP
WiFiUDP udp;

// A time instance
time_t rawTime;
time_t startTime;
time_t now;
struct tm ts;
char timeBuffer[80];
bool timeDefined = false;


///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
// SETUP
void setup() {
  Serial.begin(115200);
  pinMode(LED_BUILTIN, OUTPUT);

  wifiConnection();

  Serial.println("Starting UDP");
  udp.begin(localPort);
  Serial.print("Local port: ");
  Serial.println(udp.localPort());
  
  Serial.println("RF24 Rx Starting");
  radio.begin();
//  radio.setAutoAck(1);                    // Ensure autoACK is enabled
//  radio.enableAckPayload();               // Allow optional ack payloads
//  radio.setRetries(0,15);                 // Smallest time between retries, max no. of retries
//  radio.setPayloadSize(1); // Here we are sending 1-byte payloads to test the call-response speed
//  radio.setCRCLength(RF24_CRC_8); // Use 8-bit CRC for performance
  radio.setDataRate( RF24_250KBPS );
  radio.openReadingPipe(1, thisSlaveAddress);
//  radio.openReadingPipe(1,addresses[0]);
//  radio.openReadingPipe(2,addresses[1]);
  radio.startListening();
//  radio.printDetails();
  Serial.println("RF24 Rx Listening");
}


///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
// LOOP
void loop() {

  if (WiFi.status() == WL_CONNECTED) { //Check WiFi connection status

    // Get current time from NTP
    if (!timeDefined) {
      if (!getTimeNTP()) return; // Get time from NTP servers
      timeDefined = true;
    }

    // Get Data
    getRFData();
    if(newData == false) return;
    showRFData();
    
    if (data.stationId >= 101 && data.stationId <= 112) {
      digitalWrite(LED_BUILTIN, LOW);
      setTime(); // Based on NTP and startup time
      sendToInterSCity(); // Process the data and send to InterSCity Platform 
      digitalWrite(LED_BUILTIN, HIGH);
    }
    
  } else {

    Serial.println("Error in WiFi connection. Trying to reconnect..");
    wifiConnection();
  }
  
  Serial.println();
}


///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
// METHODS

void getRFData() {
    if ( radio.available() ) {
        radio.read( (int16_t*)&data, sizeof(data) );
        if (data.stationId == 0) return;   // Requires a valid station id!
        newData = true;
        Serial.print("New data size in bytes: ");
        Serial.println(sizeof(data));
    }
}

void showRFData() {
    if (newData == true) {
        Serial.print("Data received from station: ");
        Serial.println(data.stationId);
        newData = false;
    }
}

void setTime() {
  // set current time based on statup time and NTP time
    time (&rawTime);
    now = rawTime + startTime;
    ts = *localtime(&now);
    strftime(timeBuffer, sizeof(timeBuffer), "%Y-%m-%dT%H:%M:%S.000Z", &ts);
}

void sendToInterSCity() {
// define JSON content
    // InterSCity JSON message example:
    // {"data":[{"ldr":963,"timestamp":"2018-05-17T15:01:23.000Z"}]}
    String postMessage;
    DynamicJsonBuffer jsonBuffer;
    JsonObject& root = jsonBuffer.createObject();
    JsonArray& dataArray = root.createNestedArray("data");
    JsonObject& dataObjects = dataArray.createNestedObject();

    dataObjects["date"] = String(timeBuffer);
    
    dataObjects["stationId"] = data.stationId;
    dataObjects["sampleNumber"] = data.sampleNumber;
    dataObjects["heating"] = data.pulse;
    dataObjects["ldr"] = data.ldr*10;
    dataObjects["soil_moisture"] = data.soilMoisture*10;
    float dhtTemp = data.dhtT+((float)((float)data.dhtTm/10.));
    dataObjects["temperature"] = data.dhtT+((float)((float)data.dhtTm/10.));
    
    dataObjects["humidity"] = data.dhtH;

//    dataObjects["ads_0_1"] = data.adc0;
//    dataObjects["ads_2_3"] = data.adc1;
//    dataObjects["adc2_0_1"] = data.adc2;
//    dataObjects["adc2_2_3"] = data.adc3;
    
    dataObjects["ads_0_1"] = ((float)(dhtTemp + ((float)data.adc0) / 10.0));
    dataObjects["ads_2_3"] = ((float)(dhtTemp + ((float)data.adc1) / 10.0));
    dataObjects["ads2_0_1"] = ((float)(dhtTemp + ((float)data.adc2) / 10.0));
    dataObjects["ads2_2_3"] = ((float)(dhtTemp + ((float)data.adc3) / 10.0));
        
    root.prettyPrintTo(Serial);
    
    // define post message
    root.printTo(postMessage);
  
  // Post a message to InterSCity
    Serial.println("\nNew HTTP Post Request to InterSCity Platform");

    // define the post URL for InterSCity
    // http://143.107.45.126:30134/adaptor/resources/{uuid}/data/{resource}
    String address = String("http://")
                     + String(host) + ":"
                     + String(hostPort)
                     + "/adaptor/resources/"
                     + String(uuid)
                     + "/data/"
                     + String(resource);

    HTTPClient http;
    http.setTimeout(1000);
    Serial.print("Host address: ");
    Serial.println(address);
    http.begin(address);

    http.addHeader("Content-Type", "application/json");
    Serial.print("JSON message size in bytes: ");
    Serial.println(sizeof(postMessage));
    int httpCode = http.POST(postMessage);  //Send the request
    String payload = http.getString();      //Get the response payload

//    Serial.print("JSON message:");
//    Serial.println(postMessage); //Print JSON message
    Serial.print("HTTP return code: ");
    Serial.println(httpCode);    //Print HTTP return code
    Serial.print("Payload: ");
    Serial.println(payload);     //Print request response payload

    http.end();  //Close connection
}

////////////////////////////////////////////////////////////////////////////////////
// Useful methods

// Method to (re)connect to WiFi network
void wifiConnection() {

  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    digitalWrite(LED_BUILTIN, LOW);
    delay(500);
    digitalWrite(LED_BUILTIN, HIGH);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

}

// Method to get the NTP response and backup the *startTime*
bool getTimeNTP() {
  Serial.println("Get time from NTP servers");
  //get a random server from the pool
  WiFi.hostByName(ntpServerName, timeServerIP);

  sendNTPpacket(timeServerIP); // send an NTP packet to a time server
  // wait to see if a reply is available
  delay(1000);

  int cb = udp.parsePacket();
  if (!cb) {
    Serial.println("no NTP packet yet");
    delay(10000);
    return false;
  }
  else {
    Serial.print("NTP packet received, length=");
    Serial.println(cb);
    // We've received a packet, read the data from it
    udp.read(packetBuffer, NTP_PACKET_SIZE); // read the packet into the buffer

    //the timestamp starts at byte 40 of the received packet and is four bytes,
    // or two words, long. First, esxtract the two words:
    unsigned long highWord = word(packetBuffer[40], packetBuffer[41]);
    unsigned long lowWord = word(packetBuffer[42], packetBuffer[43]);

    // combine the four bytes (two words) into a long integer
    // this is NTP time (seconds since Jan 1 1900):
    unsigned long secsSince1900 = highWord << 16 | lowWord;

    // now convert NTP time into everyday time:
    // Unix time starts on Jan 1 1970. In seconds, that's 2208988800:
    const unsigned long seventyYears = 2208988800UL;

    // subtract seventy years:
    //unsigned long epoch = secsSince1900 - seventyYears;
    now = secsSince1900 - seventyYears;

    // backup time after seventy years
    time( &rawTime );
    startTime = now - rawTime;

    Serial.println("Time defined");

    return true;
  }
}

// Method to send an NTP request to the time server at the given address
void sendNTPpacket(IPAddress& address)
{
  Serial.println("sending NTP packet...");
  // set all bytes in the buffer to 0
  memset(packetBuffer, 0, NTP_PACKET_SIZE);
  // Initialize values needed to form NTP request
  // (see URL above for details on the packets)
  packetBuffer[0] = 0b11100011;   // LI, Version, Mode
  packetBuffer[1] = 0;     // Stratum, or type of clock
  packetBuffer[2] = 6;     // Polling Interval
  packetBuffer[3] = 0xEC;  // Peer Clock Precision
  // 8 bytes of zero for Root Delay & Root Dispersion
  packetBuffer[12]  = 49;
  packetBuffer[13]  = 0x4E;
  packetBuffer[14]  = 49;
  packetBuffer[15]  = 52;

  // all NTP fields have been given values, now
  // you can send a packet requesting a timestamp:
  udp.beginPacket(address, 123); //NTP requests are to port 123
  udp.write(packetBuffer, NTP_PACKET_SIZE);
  udp.endPacket();
}

