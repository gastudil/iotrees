/*
 Based on:
   Arduino example AnalogOutSerial.ino
   Lux sensor: https://github.com/claws/BH1750
   DHT 22: http://www.ardumotive.com/how-to-use-dht-22-sensor-en.html
   ADS1115: http://henrysbench.capnfatz.com/henrys-bench/arduino-voltage-measurements/arduino-ads1115-module-getting-started-tutorial/
   RF24 Tx: http://forum.arduino.cc/index.php?topic=421081.msg2899627#msg2899627
*/

#include <Wire.h>
#include <dht.h>
#include <Adafruit_ADS1015.h>
#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>

#define __DEBUG__ false
char *sep = ","; // Print separator

const int LED_H_PIN = 3;
const int LED_RF_PIN = 5;
long randNumber;

///////////////////////////////////////////////////////////////////
// RF24
#define STATION_ID 102 // Change this value and print it on the station (101,112)
#define CE_PIN   9
#define CSN_PIN 10

const byte slaveAddress[5] = {'R','x','I','o','T'};

RF24 radio(CE_PIN, CSN_PIN); // Create a Radio

typedef struct StationData {
  uint8_t stationId;
  uint8_t sampleNumber;
  uint8_t pulse;
  uint8_t ldr;
  uint8_t soilMoisture;
  int8_t dhtT;
  int8_t dhtTm;
  int8_t dhtH;
  int8_t adc0;
  int8_t adc1;
  int8_t adc2;
  int8_t adc3;
};

StationData data;

////////////////////////////////////////////////////////////////////////
// Sensors

// LDR
const int LDR_PIN = A0;  // Analog input pin that the LDR is attached to

// Soil moisture
const int SOIL_MOISTURE_PIN = A1;

// DHT 22
dht DHT;
const int DHT22_PIN = 8;

// ADS
adsGain_t selectedAdsGain;
Adafruit_ADS1115 ads;         /* Use this for the 16-bit version */
Adafruit_ADS1115 ads2(0x49);  /* Use this for the 16-bit version */
int adsGainList[] = { GAIN_TWOTHIRDS, GAIN_ONE, GAIN_TWO, GAIN_FOUR, GAIN_EIGHT, GAIN_SIXTEEN };
float scales[] = { 0.1875, 0.125, 0.0625, 0.03125, 0.015625, 0.0078125 };
int gainIdx = 4;
  
  // The ADC input range (or gain) can be changed via the following
  // functions, but be careful never to exceed VDD +0.3V max, or to
  // exceed the upper and lower limits if you adjust the input range!
  // Setting these values incorrectlanalogIny may destroy your ADC!
  //                                                                ADS1015  ADS1115            value
  //                                                                -------  -------            -----
  // ads.setGain(GAIN_TWOTHIRDS);  // 2/3x gain +/- 6.144V  1 bit = 3mV      0.1875mV (default) 0
  // ads.setGain(GAIN_ONE);        // 1x gain   +/- 4.096V  1 bit = 2mV      0.125mV            512
  // ads.setGain(GAIN_TWO);        // 2x gain   +/- 2.048V  1 bit = 1mV      0.0625mV           1024
  // ads.setGain(GAIN_FOUR);       // 4x gain   +/- 1.024V  1 bit = 0.5mV    0.03125mV          1536
  // ads.setGain(GAIN_EIGHT);      // 8x gain   +/- 0.512V  1 bit = 0.25mV   0.015625mV         2048
  // ads.setGain(GAIN_SIXTEEN);    // 16x gain  +/- 0.256V  1 bit = 0.125mV  0.0078125mV        2560

//////////////////////////////////////////////////////////////////

// Heating pulse monitor
const int GATE_PIN = 2;
int pulse = LOW;

// ADS1115 temperature conversion
float t0 = 1.3500000E+02;
float v0 = 5.9588600E+00;
float p[] = { 2.0325591E+01, 3.3013079E+00, 1.2638462E-01, -8.2883695E-04 };
float q[] = { 1.7595577E-01, 7.9740521E-03, 0.0 };

// FUNCTION X (MISSING REFERENCE)
float calc(float v){
    float d1 = 0.;
    float d2 = 1.;
    float t = 0;
    t = v - v0;
    for (int a = 0; a < 4; a++) {
        d1 += t * p[a];
        t = t * (v - v0);
    }
    t = v - v0;
    for (int b = 0; b < 3; b++) {
        d2 += t * q[b];
        t = t * (v - v0);
    }
    return (t0 + d1/d2);
}

// ALGORITHM

unsigned long currentMillis;
unsigned long prevMillis = 0;

// SETUP
unsigned long intervalMillisTest = 10000;         // 1 - Test the resistance: heating for 10 seconds
unsigned long intervalMillisSetupStandby = 60000; // 2 - Standby for 1 min (resting time)

// LOOP
unsigned long intervalMillisSamples = 2000;       // 0 - Start collecting the data every 2 seconds
unsigned long sampleNumber = 1;                   //     Initial sample = 1
int lastSampleHeaderData = 5;                     // 1 - Collect data for 10 seconds (header data)
int lastSampleHeating = lastSampleHeaderData + 7; // 2 - Heat for 14 seconds 
                                                  //     Based on E=Pt=Uit using current voltage and resistance values
int lastSampleTailData = lastSampleHeating + 50;  // 3 - Finish collecting data (100 seconds)
StationData dataArray[63];                        // 4 - Send the data (62 packets). 63 = 1 + 5 + 7 + 50 (Based on 0,1,2,3)
boolean standby = false;                          // 5 - Standby for 30 minutes (1800000L)
unsigned long intervalMillisLoopStandby = 1200000;

///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
// SETUP
void setup() {

  Serial.begin(115200);

  Serial.println("IoTrees Sensing");
  Serial.print("Station: #");
  Serial.println(STATION_ID);
  
// RF24
  Serial.println("Radio starting");
  pinMode(LED_RF_PIN, OUTPUT);
  radio.begin();
//  radio.setAutoAck(1);             // Ensure autoACK is enabled
//  radio.enableAckPayload();        // Allow optional ack payloads
//  radio.setRetries(0,15);          // Smallest time between retries, max no. of retries
//  radio.setPayloadSize(1);         // Here we are sending 1-byte payloads to test the call-response speed
//  radio.setCRCLength(RF24_CRC_8);  // Use 8-bit CRC for performance
  radio.setDataRate( RF24_250KBPS );
  radio.setRetries(3,5);             // delay, count
  
  radio.openWritingPipe(slaveAddress);
  radio.printDetails();  
  Serial.println("Radio started");

// LDR
  pinMode (LDR_PIN, INPUT);
  
// Moisture
  pinMode (SOIL_MOISTURE_PIN, INPUT);

// DHT
//  pinMode (DHT22_PIN, INPUT);

// ADS
  ads.setGain(adsGainList[gainIdx]);
  ads2.setGain(adsGainList[gainIdx]);
  
  ads.begin();
  ads2.begin();

// Heating
  pinMode(GATE_PIN, OUTPUT); // heating pulse
  pinMode(LED_H_PIN, OUTPUT);

// ALGORITHM: SETUP
  Serial.println("Heating setup started");
  pulse = HIGH;
  digitalWrite(GATE_PIN, pulse);
  digitalWrite(LED_H_PIN, pulse);
  delay(intervalMillisTest);
  pulse = LOW;
  digitalWrite(GATE_PIN, pulse);
  digitalWrite(LED_H_PIN, pulse);
  delay(intervalMillisSetupStandby);  
  Serial.println("Heating setup finished");
}


///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
// LOOP
void loop() {
  currentMillis = millis();
  
  if ( standby == true ) {
    if (currentMillis - prevMillis >= intervalMillisLoopStandby) {
      sampleNumber = 1;
      standby = false;
      Serial.println("STAND BY OFF. Hello.");
      prevMillis = millis();
    }
  } else if (currentMillis - prevMillis >= intervalMillisSamples) {
    
    if (sampleNumber <= lastSampleHeaderData) {
      getData();
      printData();
    } else if (sampleNumber <= lastSampleHeating) {
      pulse = HIGH;
      digitalWrite(GATE_PIN, pulse);
      digitalWrite(LED_H_PIN, pulse);
      getData();
      printData();
      if (sampleNumber == lastSampleHeating) {
        pulse = LOW;
        digitalWrite(GATE_PIN, pulse);
        digitalWrite(LED_H_PIN, pulse);
      }
    } else if (sampleNumber <= lastSampleTailData) {
      getData();
      printData();
    } else {
      // Send data
      send();
      standby = true;
      Serial.println("STAND BY ON. Good bye.");
    }
    sampleNumber++;    
    prevMillis = millis();    
  }  
}

///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
// METHODS

void getData() {
  data.stationId = (uint8_t) STATION_ID;
  data.sampleNumber = (uint8_t) sampleNumber;
  data.pulse = (uint8_t) pulse;

  data.ldr = (uint8_t) (analogRead(LDR_PIN)/10);
  data.soilMoisture = (uint8_t) (analogRead(SOIL_MOISTURE_PIN)/10);

  int chk = DHT.read22(DHT22_PIN);
  switch (chk)
  {
    case DHTLIB_OK:
      data.dhtT = (int8_t) (DHT.temperature);
      data.dhtTm = (int8_t) ((DHT.temperature-((float)data.dhtT))*10.);
      data.dhtH = (int8_t) DHT.humidity;
      break;
    default: 
      data.dhtH = (int8_t) -1;
      data.dhtTm = (int8_t) 0;
      data.dhtT = (int8_t) -1;
      break;
  }

  // differential  
  float adc0 = ads.readADC_Differential_0_1();
  float adc1 = ads.readADC_Differential_2_3();
  float adc2 = ads2.readADC_Differential_0_1();
  float adc3 = ads2.readADC_Differential_2_3();
 
  float dadc0 = (10.0 * calc(adc0) * scales[gainIdx]);
  float dadc1 = (10.0 * calc(adc1) * scales[gainIdx]);
  float dadc2 = (10.0 * calc(adc2) * scales[gainIdx]);
  float dadc3 = (10.0 * calc(adc3) * scales[gainIdx]);

  data.adc0 = (int8_t) dadc0;
  data.adc1 = (int8_t) dadc1;
  data.adc2 = (int8_t) dadc2;
  data.adc3 = (int8_t) dadc3;

  dataArray[sampleNumber] = data;
}

//====================

void printData() {
  Serial.println("Data:");

  Serial.print("sId");
  Serial.print(",\t");
  Serial.print("sN");
  Serial.print(",\t");
  Serial.print("pls");
  Serial.print(",\t");
  Serial.print("ldr");
  Serial.print(",\t");
  Serial.print("sMo");
  Serial.print(",\t");
  Serial.print("dhtT");
  Serial.print(",\t");
  Serial.print("dhtTm");
  Serial.print(",\t");
  Serial.print("dhtH");
  Serial.print(",\t");
  Serial.print("adc0");
  Serial.print(",\t");
  Serial.print("adc1");
  Serial.print(",\t");
  Serial.print("adc2");
  Serial.print(",\t");
  Serial.println("adc3");
  
  Serial.print(data.stationId);
  Serial.print(",\t");
  Serial.print(data.sampleNumber);
  Serial.print(",\t");
  Serial.print(data.pulse);
  Serial.print(",\t");
  Serial.print(data.ldr);
  Serial.print(",\t");
  Serial.print(data.soilMoisture);
  Serial.print(",\t");
  Serial.print(data.dhtT);
  Serial.print(",\t");
  Serial.print(data.dhtTm);
  Serial.print(",\t");
  Serial.print(data.dhtH);
  Serial.print(",\t");
  Serial.print(data.adc0);
  Serial.print(",\t");
  Serial.print(data.adc1);
  Serial.print(",\t");
  Serial.print(data.adc2);
  Serial.print(",\t");
  Serial.println(data.adc3);
}

// Send all data collected
void send() {
  bool rslt;
  int currentSample = 1;
  
  Serial.println("Sending data..");
  
  Serial.print("Packet size in bytes: ");
  Serial.println(sizeof(data));
  Serial.print("Total data size in bytes: ");
  Serial.println(sizeof(dataArray));

  do {
    Serial.print("Sample number: #");
    Serial.print(currentSample);
    digitalWrite(LED_RF_PIN, HIGH);
    rslt = radio.write( &dataArray[currentSample], sizeof(dataArray[currentSample]) );
    if (rslt) {
        Serial.println(", Acknowledge received.");
        currentSample++;
    }
    else {
        Serial.println(", Tx failed.");
    }
    digitalWrite(LED_RF_PIN, LOW);
    randNumber = random(500,1000);
    delay(randNumber);
  } while (currentSample <= lastSampleTailData);
  
  Serial.println("Data sent!"); 
}
